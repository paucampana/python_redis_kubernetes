import os

from flask import Flask
from redis import Redis


app = Flask(__name__)
redis = Redis(host=os.environ['REDIS_HOST'], port=os.environ['REDIS_PORT'])
bind_port = int(os.environ['BIND_PORT'])


@app.route('/')
def hello():


    numberList = "dates_test"
    
        
    list_redis = []
    for i in range(0, redis.llen(numberList)):
        my_v = redis.lindex(numberList, i)
        list_redis.append(my_v)


    return f'Hello from Redis! This is your list of ids: {list_redis} '


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=bind_port)
