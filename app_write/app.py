import os

from flask import Flask
from redis import Redis
import datetime
import socket


app = Flask(__name__)
redis = Redis(host=os.environ['REDIS_HOST'], port=os.environ['REDIS_PORT'])
bind_port = int(os.environ['BIND_PORT'])


@app.route('/')
def hello():
    
    
    # Create a Redis list with all zeros
    numberList = "dates_test"
    actual_date = socket.gethostbyname(socket.gethostname())
    redis.lpush(numberList, actual_date)
        
    # Print the Redis list after modification
    list_redis = []
    for i in range(0, redis.llen(numberList)):
        my_v = redis.lindex(numberList, i)
        list_redis.append(my_v)


    # Empty the Redis list
    #for i in range(0, redis.llen(numberList)):
    #    redis.lpop(numberList)

    
    return f'MY IP: {actual_date} to REDIS!'


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=bind_port)
